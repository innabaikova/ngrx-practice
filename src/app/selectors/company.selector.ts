import {AppState} from '../models/appState';
import {createSelector} from '@ngrx/store';

export const selectCompanies = (state: AppState) => state.companies.companies;

export const getCompanyById = (id: number) => createSelector(selectCompanies, (allCompanies) => {
  if (allCompanies) {
    return allCompanies.find(item => {
      return item.id === id;
    });
  } else {
    return {};
  }
});
