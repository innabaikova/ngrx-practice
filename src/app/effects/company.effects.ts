import {Injectable} from '@angular/core';
import {CompanyService} from '../company/company.service';
import {Actions, Effect} from '@ngrx/effects';
import * as companyActions from './../actions/company.actions';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Action} from '@ngrx/store';

@Injectable()
export class CompanyEffects {
  @Effect() loadCompanies$: Observable<Action> = this.actions$
    .ofType(companyActions.LOAD_COMPANIES)
    .switchMap(() => {
      return this.companyService.loadCompanies()
        .map(companies => new companyActions.LoadCompaniesSuccessAction(companies));
    });

  @Effect() deleteCompany$: Observable<Action> = this.actions$
    .ofType(companyActions.DELETE_COMPANY)
    .switchMap((action: companyActions.DeleteCompanyAction) => {
      return this.companyService.deleteCompany(action.payload)
        .map(company => new companyActions.DeleteCompanySuccessAction(company.id));
    });

  @Effect() addCompany$: Observable<Action> = this.actions$
    .ofType(companyActions.ADD_COMPANY)
    .switchMap((action: companyActions.AddCompanyAction) => {
      return this.companyService.addCompany(action.payload)
        .map(company => new companyActions.AddCompanySuccessAction(company))
        .do(() => this.router.navigate([`/company/list`]));
    });

  @Effect() updateCompany$: Observable<Action> = this.actions$
    .ofType(companyActions.UPDATE_COMPANY)
    .switchMap((action: companyActions.UpdateCompanyAction) => {
      return this.companyService.updateCompany(action.payload)
        .map(company => new companyActions.UpdateCompanySuccessAction(company))
        .do(() => this.router.navigate([`/company/list`]));
    });

  constructor(private actions$: Actions,
              private companyService: CompanyService,
              private router: Router) {
  }
}
