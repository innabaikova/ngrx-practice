import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CompanyService } from '../company.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as companyActions from './../../actions/company.actions';
import { AppState } from './../../models/appState';
import {getCompanyById} from '../../selectors/company.selector';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss']
})
export class CompanyEditComponent implements OnInit {
  companyForm: FormGroup;
  isNewCompany: boolean;
  companyId: any;

  constructor(
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {

  }

  ngOnInit() {
    this.companyId = this.activatedRoute.snapshot.params['id'];
    this.isNewCompany = this.companyId === 'new';
    this.buildForm();
    if (!this.isNewCompany) {
      this.getCompany();
    }

  }

  getCompany() {
    const id = +this.companyId;

    this.store.select(getCompanyById(id))
      .subscribe(company => this.companyForm.patchValue(company));
 }

  buildForm() {
    this.companyForm = this.fb.group({
      name: ['', Validators.required],
      email: [''],
      phone: ['']
    });
  }

  saveCompany() {
    if (this.isNewCompany) {
      this.store.dispatch(new companyActions.AddCompanyAction(this.companyForm.value));
    } else {
      const companyToUpdate = { ...this.companyForm.value, id: this.companyId };
      this.store.dispatch(new companyActions.UpdateCompanyAction(companyToUpdate));
    }
  }

}
